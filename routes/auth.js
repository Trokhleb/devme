module.exports = (app) => {
    app.get('/auth/login', (req, res) => {
        res.send('login');
    });
    app.get('/auth/logout', (req, res) => {
        res.send('logout');
    });
    app.get('/auth/google', (req, res) => {
        res.send('google auth');
    });
}