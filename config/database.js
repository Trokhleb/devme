module.exports = (mongoose, config) => {
    const database = mongoose.connection;
    mongoose.Promise = Promise;
    mongoose.connect(config.database, {
        promiseLibrary: global.Promise
    });
    
    database.on('error', (error) => console.log(`Connection to devMe database failed: ${error}`));
    database.on('connected', () => console.log("Connected to devMe database."));
    database.on('disconnected', () => console.log("Disconnected from devMe database."));
    
}