module.exports = {
    secret: 'devmesecret',
    session: { session: false },
    database: 'mongodb://127.0.0.1:27017/devme',
    port: '3000'
}