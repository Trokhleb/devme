const express = require('express'),
      app = express(),
      bodyParser = require('body-parser'),
      mongoose = require('mongoose'),
      passport = require('passport'),
      jwt = require('jsonwebtoken'),
      config = require('./config/index.js'),
      path = require('path'),
      consign = require('consign'),
      database = require('./config/database.js')(mongoose, config),
      helmet = require('helmet');

app.use(express.static(path.join(__dirname, '/media')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(helmet());

app.set('devmesecret', config.secret);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');

consign().include('./routes').into(app);

app.listen(config.port, () => console.log(`devMe project is running on port: ${config.port}`));

